

const express = require("express");
const router = express.Router()
const { dirname } = require('path');
router.use(express.static("public"));
const bodyParser = require('body-parser');

const sqlite3 = require('sqlite3').verbose();


const database ={
    eancode: '',
    aantal:'',
    plank:'',
    hoogte:'',
    productname:'',
    eenheid:'',
}

 // Use body-parser middleware to parse form data
router.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
router.use(bodyParser.json());

router.get("/:title", function(req ,res)        // with this function you print the value from the url on the webpage
{
  const Title = req.params.title;
  res.render("article.ejs",{
    title: Title});
});

router.get("/", function(req ,res)
{
  const Title = req.params.title;
  eancode = ``
 // res.render("article.ejs",{eancode});
  res.render("article.ejs",database);

});




router.post('/', (req, res) => 
{
    const db = new sqlite3.Database('artbeheerV2.db');
    eancode = req.body.eancode;
    
    db.get('SELECT * FROM Artikelen WHERE eancode = ?', [eancode], function(err, row) {
        if (err) {
          console.error(err.message);
        }
        else {
            if (row) {
              // eancode already exists in the database, update the row
              console.log(`eancode ${eancode} already exists`);
              // update query here
            } else {
              // eancode does not exist in the database, insert the new row
              console.log(`eancode ${eancode} does not exist`);
              // insert query here
            }
        }
        
        res.render('article.ejs',{eancode,row} );
        console.log(eancode);
      });
    

});


router.get('/', function(req, res) {
    res.set('Content-Type', 'text/css');
    res.render('')
  });

/*

  

router.post('/', (req, res) => {
    //res.sendFile(__dirname + '/artikelen.html');
   res.send({data: "user created"});
  });  
*/


//db.run('CREATE TABLE hallo (id INT, name TEXT)');




console.log('Creating table');

//console.log(myValue);


module.exports = router;

